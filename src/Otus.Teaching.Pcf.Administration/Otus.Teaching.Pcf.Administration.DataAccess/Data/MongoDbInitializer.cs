﻿using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private IMongoCollection<Employee> _employeeCollection;
        private IMongoCollection<Role> _roleCollection;

        public MongoDbInitializer(IMongoDbSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            client.DropDatabase(settings.Database);
            var database = client.GetDatabase(settings.Database);
            _employeeCollection = database.GetCollection<Employee>("Employees");
            _roleCollection = database.GetCollection<Role>("Roles");
        }

        public void InitializeDb()
        {
            
            _employeeCollection.InsertMany(FakeDataFactory.Employees);
            _roleCollection.InsertMany(FakeDataFactory.Roles);
        }
    }
}