﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public interface IMongoDbSettings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }

    public class MongoDbSettings : IMongoDbSettings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
