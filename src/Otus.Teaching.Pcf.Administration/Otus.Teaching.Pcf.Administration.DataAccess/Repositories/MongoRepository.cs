﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T> : IMongoRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _entityCollection;
        private string _collectionName = "";

        public MongoRepository(IMongoDbSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.Database);
            switch (typeof(T).FullName)
            {
                case "Otus.Teaching.Pcf.Administration.Core.Domain.Administration.Employee":
                    _collectionName = "Employees";
                    break;
                case "Otus.Teaching.Pcf.Administration.Core.Domain.Administration.Role":
                    _collectionName = "Roles";
                    break;
            }

            _entityCollection = database.GetCollection<T>(_collectionName);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _entityCollection.Find(entity => true).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _entityCollection.Find<T>(entity => entity.Id == id).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            var count = _entityCollection.ReplaceOne<T>(obj => obj.Id == entity.Id, entity).ModifiedCount;
            return;
        }


        Task<IEnumerable<T>> IMongoRepository<T>.GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        Task<T> IMongoRepository<T>.GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<T>> IMongoRepository<T>.GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        Task IMongoRepository<T>.AddAsync(T entity)
        {
            throw new NotImplementedException();
        }


        Task IMongoRepository<T>.DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
